# AlayaCare frontend development test with VueJS framework

The test we're giving you comes from the daily UI challenge website (http://www.dailyui.co/).

The challenge is the following:
- Design a credit card checkout form page.

Because this is a challenge made for designers and not developers, we invite you to take a look at this page:
- https://dribbble.com/search?q=Daily+UI+%23002+Card+Checkxout

## Requirements
- Pick the design you like the most, and develop it.
  - Design picked: 
    - Source: https://dribbble.com/shots/5969622-ZAZ-purchase/
    - Local: ./src/assets/credit_card_checkout.png
- Please use Vue.js, you can even use the vue-cli to start your project (https://github.com/vuejs/vue-cli)
- You are free to use any CSS library you want (from bootstrap to semantic UI to a material design based library). 
  - You can even use a CSS library for layout and a different one from the form inputs and buttons.
- If you decide to use a layout that has a carousel for credit displaying for example, use an existing library (check out https://github.com/vuejs/awesome-vue).
- No backend here, no need for Vuex either. It's ok if we refresh the page and lose our changes.

## The key things we will be looking at are the following:
- Components - How will you split your UI elements into components, how will you design them? How will you name them?
- Form Validation - Again, any library you want. How will you handle errors when saving the credit card information?
- Tests - Write a couple of tests for the form validation, make sure no incorrect information is 'saved'
- CSS (you can use LESS or SASS, or even regular CSS) - How you will split it?

Once you're done, put it up on github, and make sure we can easily run it if we download it (by running commands like npm start or npm test). Remember, if you use a boilerplate / CLI, you won't waste time configuring these things (keep it simple).

## Project Infos:
- GitLab for versioning
- GitLab CI tool to continuously deploy on AWS S3 bucket (see .gitlab-ci.yml)
- Repo: https://gitlab.com/chinh-dev11/test-alayacare-vue
- Url: test-alayacare-vue.chinhle.ca

---

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
