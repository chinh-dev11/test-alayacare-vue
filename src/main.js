import Vue from 'vue';
import Vuelidate from 'vuelidate';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import WebFont from 'webfontloader';
import App from './App.vue';

// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap-vue/dist//bootstrap-vue.css';
// import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css';
// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap-vue/dist/bootstrap-vue.css';
import './style/main.scss';

WebFont.load({
  google: {
    families: ['Noto Sans'],
  },
});

// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(BootstrapVueIcons);

Vue.use(Vuelidate);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');
