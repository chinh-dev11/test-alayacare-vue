const GoogleFontsPlugin = require('google-fonts-webpack-plugin');

module.exports = {
  configureWebpack: {
    plugins: [
      new GoogleFontsPlugin({
        fonts: [
          { family: 'Roboto', variants: ['400', '700italic'] },
          { family: 'Raleway', variants: ['400', '500', '700'] },
        ],
      }),
    ],
  },
};
